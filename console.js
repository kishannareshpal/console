
// Initialize Firebase
  var config = {
    apiKey: "AIzaSyBsqI-AGvHpvdaDyvMQhSJJE9Pt01VgN5c",
    authDomain: "examesproject.firebaseapp.com",
    databaseURL: "https://examesproject.firebaseio.com",
    projectId: "examesproject"
  };
  firebase.initializeApp(config);
  var db = firebase.database();




$(document).ready(function(){
   // On ENTER (keycode 13) Press while on PasswordInput simulate a click on the Autenticar Button.
   $('#p_in').keypress(function(e){
     if(e.keyCode==13)
     $('#go').click();
   });

   $('#k-input').keypress(function(e){
     if(e.keyCode==13)
     $('#r-user').click();
   });

   iziToast.settings({
      timeout: 5000,
      theme: "dark",
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX'
    });

    $("#go").click(function() {
      var e_in = document.getElementById('e_in').value;
      var p_in = document.getElementById('p_in').value;

      if ((e_in || p_in) === "" ) {
        iziToast.show({
          theme: "dark",
          message: "Dude wake up, you haven't written anything in the text field. C'mon."
        });
      } else {
        var promise = firebase.auth().signInWithEmailAndPassword(e_in, p_in);
            // If error occors, log it.
            promise.catch(function(e){
              console.log(e);
              // If invalid email
              if(e.code === "auth/invalid-email"){
                iziToast.show({
                  timeout: 3000,
                  message: "Incorrect ExamesMozDevID. You are not allowed in this page!",
                  onClosed: function (instance, toast, closedBy) {
                    window.location.replace("http://www.mozexames.com");
                  }
                });

              // If invalid Password
              } else if(e.code === "auth/wrong-password"){
                iziToast.show({
                  theme: "dark",
                  message: "Wrong Password. Please check again."
                });

              // If invalid user or User not registered
              } else if (e.code === "auth/user-not-found") {
                iziToast.show({
                  timeout: 3000,
                  message: "Incorrect ExamesMozDevID. You are not allowed in this page!",
                  onClosed: function (instance, toast, closedBy) {
                    window.location.replace("http://www.mozexames.com");
                  }
                });
              } else {
                iziToast.show({
                  timeout: 3000,
                  message: "Starting Console...!",
                  onClosing: function (instance, toast, closedBy) {

                  }
                });
              }
            });
      }
    });//go on click
      if(Cookies.get('u_in') == "yes"){
        $('#non-authenticated').addClass('hidden');
        $('#authenticated').removeAttr('hidden');
        $('#authenticated').addClass('animated fadeIn');
        iziToast.show({
          timeout: 2000,
          overlay: true,
          position: "bottomLeft",
          title: "Hi Kishan",
          message: "Welcome back!"
        })
      } else {

        //RealTime login/logout listener
      firebase.auth().onAuthStateChanged(function(firebaseUser){
        if(firebaseUser){

          //If the user is logged in do this
          //Save a cookie
          document.cookie = "u_in=yes; expires=Thu, 23 Nov 2020 12:00:00 UTC; path=/";
          iziToast.show({
            timeout: 2000,
            overlay: true,
            position: "bottomLeft",
            title: "Hi " + firebaseUser.displayName,
            message: "Welcome back!",
            onOpening: function (instance, toast, closedBy) {
                $('#non-authenticated').addClass('animated fadeOutUp');
                $('#authenticated').addClass('animated fadeIn');
                $('#authenticated').removeAttr('hidden');
            }
          });



        } else {
          //If the user is NOT logged in do this
          document.cookie = "u_in=yes; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        }
      });
      }


    $("#r-user").click(function() {
        var pn_in = document.getElementById('pn-input').value;
        var k_in = document.getElementById('k-input').value;

        if((k_in && pn_in) == ""){
          iziToast.show({
            theme: "light",
            message: "Please fill in the blacks"
          });
        } else {
          db.ref('paid_users/' + pn_in).set({
            "key": k_in.toLowerCase()
          });

          iziToast.show({
            theme: "light",
            message: "User registered successfully!"
          });

          return db.ref('paid_users/' + pn_in).once('value').then(function(snapshot) {

            document.getElementById('pn_text').innerHTML = "Ola caro estudante, muito obrigado pela sua contribuição. A sua chave é: '" + snapshot.val().key + "'.%0D%0A %0D%0AEquipe Exames[mz]";
            $("#c-user").removeAttr('disabled');
            $("#r-user").attr('disabled', '');

            if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream){
              var t = "&";
            }else {
              var t = "?"
            };

            $("#c-user").attr("href", "sms:/" + pn_in + "/" + t + "body=" + pn_text.innerHTML + "");

          });
        }
    });


});
